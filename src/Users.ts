import { DateTime } from 'Luxon';
export class User {
  id: string;
  platform: string;
  username: string;
  premium: boolean;
  active: boolean;
  watchTimePoints: number;
  lastMsgTime: DateTime;
  lifetimeWatchTimePoints: number;
  lifetimeMoney: number;

  constructor(platformName: string, username: string, lastMsgTime: DateTime, premium?: boolean) {
    this.id = username + '@' + platformName;
    this.platform = platformName;
    this.username = username;
    this.premium = premium;
    this.lastMsgTime = lastMsgTime;
    this.watchTimePoints = 0;
    this.lifetimeMoney = 0;
    this.lifetimeWatchTimePoints = 0;
    this.active = true;
  }

  static from(userRecord: any) {
    const user = new User(userRecord.platform, userRecord.username, userRecord.lastMsgTime, userRecord.premium);
    user.lastMsgTime = DateTime.fromISO(userRecord.lastMsgTime);
    user.watchTimePoints = userRecord.watchTimePoints;
    user.lifetimeMoney = userRecord.lifetimeMoney;
    user.lifetimeWatchTimePoints = userRecord.lifetimeWatchTimePoints;
    user.active = userRecord.active;
    return user;
  }

  spendPoints(points: number) {
    this.watchTimePoints -= points;
    return this.watchTimePoints;
  }

  earnPoints(points: number) {
    this.lifetimeWatchTimePoints += points;
    this.watchTimePoints += points;
    return this.watchTimePoints;
  }



}