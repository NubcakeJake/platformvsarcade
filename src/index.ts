import { IChatModule } from './@types/IChatModule';
import { TwitchChatModule } from './ChatModules/TwitchChatModule';
import { YoutubeChatModule } from './ChatModules/YoutubeChatModule';
import { AsyncChessGame } from './Games/AsyncChessGame';
import { Game } from './Games/Game';

const platforms = new Map<string, IChatModule>();
let game: Game;


async function main() {
  await initialize();
  game = new AsyncChessGame(platforms.get('Twitch'), platforms.get('Youtube'));
  await game.run();
}

const initialize = async () => {
  console.log('Initializing');
  try {
    console.log('Starting Twitch Connection');
    platforms.set('Twitch', new TwitchChatModule('Twitch'));
    console.log('Starting Youtube Connection');
    platforms.set('Youtube', new YoutubeChatModule('Youtube'));
    console.log(platforms.size);

    for (const record of platforms) {
      const name = record[0];
      await platforms.get(name).connect();
      await platforms.get(name).sendMessage('Bienvenidos!');
      await platforms.get(name).getLatestMessages(true);
      if (await platforms.get(name).getState() != 'Connected') {
        console.error(`"${name}" is not connected after initialization!  Removing...`);
        platforms.delete(name);
      }
    }
  } catch (err) {
    console.log('Initialization Failed');
    console.error(err);
  }
  return;
};

main()
  .then(() => game.saveUsers())
  .catch(console.error)
  .then(() => process.exit(0));
