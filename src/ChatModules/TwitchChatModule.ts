import TwitchApi from 'node-twitch';
import TwitchChat from 'tmi.js';
import config from '../../config';
import secrets from '../../secrets.json';
import { IChatModule, Message } from '../@types/IChatModule';

export class TwitchChatModule implements IChatModule {
  name = 'Twitch';
  moduleState = 'Not Connected';
  api: TwitchApi;
  chat: TwitchChat.Client;
  messages: Message[] = [];
  msgCountRecieved = 0;
  msgCountRetrieved = 0;

  constructor(name: string) {
    this.name = name;
  }

  async connect() {
    console.log('Attempting connection to', this.name);
    this.api = new TwitchApi({
      client_id: secrets.twitchClientId,
      client_secret: secrets.twitchApiSecret
    });
    console.log('Connected to Twitch API');

    this.chat = new TwitchChat.client({
      options: { debug: true },
      identity: {
        username: config.twitchChannelName,
        password: secrets.twitchOAuthToken
      },
      channels: [config.twitchChannelName]
    });
    await this.chat.connect();
    console.log('Connected to Twitch Chat');

    try{
      this.chat.on('message', (channel, tags, message, self) => {
        if(self) return;
        this.messages.push({
          author: tags.username,
          message: message,
          premium: tags.subscriber,
          money: Number.parseInt(tags.bits)
        });
        this.msgCountRecieved++;
      });
    } catch (err) {
      console.error(err);
    }
    this.moduleState = 'Connected';
  }
    
  async getState() {
    return this.moduleState;
  }

  async getLatestMessages(advance) {
    const response = this.messages;
    this.msgCountRetrieved += response.length;
    if (advance) {
      this.messages = [];
    }
    if(this.msgCountRetrieved < this.msgCountRecieved - this.messages.length) {
      console.error('Message recieved and messages retrieved do not match!  A race condition may have dropped a message!');
    }
    return response;
  }

  async sendMessage(msg) {
    await this.chat.say(config.twitchChannelName, msg);
  }


}









