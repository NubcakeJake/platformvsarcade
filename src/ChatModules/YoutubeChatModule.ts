import fs from 'fs';
import readline from 'readline';
import { google } from 'googleapis';
import { DateTime } from 'luxon';
import secrets from '../../secrets.json';
import { IChatModule } from '../@types/IChatModule';



export class YoutubeChatModule implements IChatModule {
  name = 'Youtube';
  moduleState = 'Not Connected';
    
  auth = new google.auth.OAuth2(secrets.youtubeClientId, secrets.youtubeSecret, 'http://localhost');
  service = google.youtube('v3');
  liveChatId: string;
  broadcast: any;
  pagination: string;
    
  constructor(name: string) {
    this.name = name;
    this.auth.setCredentials(secrets.youtube);
    if(DateTime.fromMillis(secrets.youtube.expiry_date) < DateTime.now()) {
      this.oAuthHelper(this.auth).catch(console.error);
    }

  }

  private async oAuthHelper(auth) {
    const authUrl = this.auth.generateAuthUrl({
      access_type: 'offline',
      scope: ['https://www.googleapis.com/auth/youtube']
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    rl.question('Enter the code from that page here: ', function(code) {
      rl.close();
      auth.getToken(code, function(err, token) {
        if (err) {
          console.log('Error while trying to retrieve access token', err);
          return;
        }
        auth.credentials = token;
        try {
          delete secrets['youtube'];
          secrets['youtube'] = token;
          fs.writeFile('secrets.json', JSON.stringify(secrets), (err) => {
            if (err) throw err;
            console.log('Token stored to secrets.json');
          });
                    
        } catch (err) {
          if (err.code != 'EEXIST') {
            throw err;
          }
        }
      });
    });

    return;
  }



  async connect() {
    console.log('Attempting connection to', this.name);
    try {
      const broadcasts = await this.service.liveBroadcasts.list({
        auth: this.auth,
        part: ['id', 'snippet', 'contentDetails', 'status'],
        mine: true
      });

      this.broadcast = broadcasts.data.items.find(({ status }) => status?.privacyStatus === 'unlisted');
      this.liveChatId = this.broadcast.snippet.liveChatId;

    } catch(err) {
      console.error('The API returned an error:', err);
      return;
    }

    const res = await this.service.liveChatMessages.list({
      auth: this.auth,
      liveChatId: this.liveChatId,
      part: ['id', 'snippet', 'authorDetails'],
      pageToken: this.pagination
    }, {} );
    this.pagination = res.data.nextPageToken;
    console.log('Connected to Youtube API');
    this.moduleState = 'Connected';
    return;
  }

  async getState() {
    return this.moduleState;
  }

  async getLatestMessages(advance) {
    const messages = [];
    const res = await this.service.liveChatMessages.list({
      auth: this.auth,
      liveChatId: this.liveChatId,
      part: ['id', 'snippet', 'authorDetails'],
      pageToken: this.pagination
    }, {} );
    for (const item of res.data.items) {
      let money = 0;
      if ('superChatDetails' in item.snippet) {
        money = Number.parseInt(item.snippet.superChatDetails.amountMicros) / 10000; //10,000 micros is 1 cent.
      }
      messages.push({
        author: item.authorDetails.displayName,
        message: item.snippet.displayMessage,
        premium: item.authorDetails.isChatSponsor,
        money: money
      });
    }
    if (advance) this.pagination = res.data.nextPageToken;
    return messages;
  }

  async sendMessage(msg) {
    await this.service.liveChatMessages.insert({
      auth: this.auth,
      part: ['snippet']
    }, {
      body: JSON.stringify({
        snippet: {
          liveChatId: this.liveChatId,
          type: 'textMessageEvent',
          textMessageDetails: {
            messageText: msg
          }
        }
      })
    });
    return;
  }


    
    
    
}


// export async function getChannel() {
//     const service = google.youtube('v3');
//
//     try {
//         const broadcasts = await service.liveBroadcasts.list({
//             auth: this.auth,
//             part: ['id', 'snippet', 'contentDetails', 'status'],
//             mine: true
//         });
//         // console.log(JSON.stringify(broadcasts.data, null, 2));
//
//         const broadcast = broadcasts.data.items.find(({ status }) => status?.privacyStatus === 'unlisted');
//         const { liveChatId } = broadcast.snippet;
//
//         // const chatMessages = await service.liveChatMessages.list({
//         //     auth,
//         //     liveChatId,
//         //     part: ['id', 'snippet', 'authorDetails']
//         // }, {});
//         // console.log(JSON.stringify(chatMessages.data, null, 2));
//
//         await service.liveChatMessages.insert({
//             auth: this.auth,
//             part: ['snippet']
//         }, {
//             body: JSON.stringify({
//                 snippet: {
//                     liveChatId,
//                     type: 'textMessageEvent',
//                     textMessageDetails: {
//                         messageText: 'Ayyyy yooooooooo! 😖 ' + Date.now()
//                     }
//                 }
//             })
//         });
//
//         // const res = await service.channels.list({
//         //     auth,
//         //     part: 'snippet,contentDetails,statistics' as any,
//         //     forUsername: config.youtubeChannelName
//         // });
//         //
//         // const channels = res?.data.items;
//         // if (!channels) {
//         //     console.log('No channel found.');
//         // } else {
//         //     console.log('This channel\'s ID is %s. Its title is \'%s\', and ' +
//         //         'it has %s views.',
//         //         channels[0].id,
//         //         channels[0].snippet?.title,
//         //         channels[0].statistics?.viewCount);
//         // }
//     } catch(err) {
//         console.error('The API returned an error:', err);
//         return;
//     }
// }
