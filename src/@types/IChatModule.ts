export interface Message {
    author: string;
    message: string;
    premium: boolean;
    points?: number;
    money?: number;
}

export interface IChatModule {
    name: string;
    connect: () => Promise<void>;
    getState: () => any;
    getLatestMessages: (advance: boolean) => Promise<Message[]>;
    sendMessage: (msg: string) => void;
}
