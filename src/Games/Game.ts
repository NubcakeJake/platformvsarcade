import fs from 'fs/promises';
import { DateTime } from 'luxon';
import savedUsers from '../../users.json';
import { IChatModule, Message } from '../@types/IChatModule';
import { User } from '../Users';

//Todo: make this class a generic game inherited by other specific game classes
export class Game {
  name: string;
  maxTeams: number;

  platforms: Record<string, IChatModule> = {};
  users = new Map<string, User>();
  pointRateBase: number; //Earning rate in points per minute
  pointRatePremium: number; //Additional earning rate in points per minute for premium users
  lastPointsAward: DateTime;

  constructor(platforms: Record<string, IChatModule>) {
    this.platforms = platforms;
    this.pointRateBase = 120;
    this.pointRatePremium = 60;
    this.lastPointsAward = DateTime.now();
    try {
      savedUsers.map(User.from).forEach((user) => {
        this.users.set(user.id, user);
      });
    } catch (err) {
      console.error(err);
      this.users.set('testDuck@Twitch', new User('Twitch', 'testDuck', DateTime.now()));
    }
  }

  run() {
    //TODO sleep or something geez
    this.tick();
  }

  tick() {
    //This will do something soon, stop yelling Mr. Linter
  }

  async processMessages() {
    const time = DateTime.now();
    this.awardPoints(time);
    for (const platformName of Object.keys(this.platforms)) {
      for (const msg of await this.platforms[platformName].getLatestMessages(true)) {
        const userId = Game.userIdHelper(msg.author, platformName);
        if(!this.users.has(userId)) {
          this.users.set(userId, new User(platformName, msg.author, time));
        } else {
          this.users.get(userId).lastMsgTime = time;
        }
        this.users.get(userId).premium = msg.premium;
        this.processCommand(platformName, msg);
      }
    }
  }

  /*
    This function processes messages for game compatible commands.  It will most likely be overridden by child class
    games for unique per game commands and behaviors.
     */
  processCommand(team: string, msg: Message) {
    let commandBreakdown: string[];
    let vars: any;
    const commandingUser = this.users.get(Game.userIdHelper(msg.author, team));
    const oldPoints = commandingUser.watchTimePoints;

    if(msg.message[0] !== '!') {
      return;
    } else {
      commandBreakdown = msg.message.split(' ', 10);
    }
    switch (commandBreakdown[0]) {
    case '!test':
      commandBreakdown.shift();
      vars = commandBreakdown.join(', ');
      console.log('Test Command with variables:', vars);
      this.platforms[team].sendMessage(`@${msg.author} Test Command with variables: ${vars}`);
      break;

    case '!test10':
      if(commandingUser.watchTimePoints > 10) {
        commandingUser.spendPoints(10);
        console.log(`Test Command, user: ${commandingUser.username} spent 10 points bringing them from ${oldPoints} to ${commandingUser.watchTimePoints} points.`);
      } else {
        console.log(`Test Command, user: ${commandingUser.username} tried to spend 10 points, but only has ${commandingUser.watchTimePoints} points.`);
      }
      break;

    case '!testX':
      vars = Number.parseInt(commandBreakdown[1]);
      if(vars) {
        if(commandingUser.watchTimePoints > vars) {
          commandingUser.spendPoints(vars);
          console.log(`Test Command, user: ${commandingUser.username} spent ${vars} points bringing them from ${oldPoints} to ${commandingUser.watchTimePoints} points.`);
        } else {
          console.log(`Test Command, user: ${commandingUser.username} tried to spend ${vars} points, but only has ${commandingUser.watchTimePoints} points.`);
        }
      } else {
        console.log(`Test Command, user: ${commandingUser.username} tried to spend variable points, but ${commandBreakdown[1]} is not a number`);
        this.platforms[team].sendMessage(`@${msg.author} arg0 for command !testX is Not A Number`);
      }
      break;

    case '!points':
      this.platforms[team].sendMessage(`@${msg.author} You have ${commandingUser.watchTimePoints} points, and you have ${commandingUser.lifetimeWatchTimePoints} lifetime points`);
      break;
    }
  }

  private awardPoints(time: DateTime) {
    const timeSinceLastPoints = time.diff(this.lastPointsAward, 'milliseconds');
    console.log(timeSinceLastPoints.toObject().milliseconds);
    this.users.forEach( (user) => {
      if(time.diff(user.lastMsgTime, 'minutes').toObject().minutes < 30 ) {
        let pointsRate = this.pointRateBase;
        if (user.premium) pointsRate += this.pointRatePremium;
        let points = pointsRate * timeSinceLastPoints.toObject().milliseconds / 60000;
        console.log('points: ', points);
        points = Math.max(1, points);
        user.earnPoints(points);
        console.log('Giving user:', user.username, points, 'points!');
        user.active = true;
      }
    });
    this.lastPointsAward = time;
  }

  addPlatform(platform: IChatModule) {
    this.platforms[platform.name] = platform;
  }

  removePlatform(platform: IChatModule) {
    this.users.forEach( (user) =>  {
      if (user.platform == platform.name) {  //This should be yelling at me, but here we are
        user.active = false;
      }
    });
    delete this.platforms[platform.name];
  }

  async saveUsers() {
    console.log('Saving the Users from their forever destruction and torment in the void!');
    await fs.writeFile('users.json', JSON.stringify(Array.from(this.users.values()), null, 2), 'utf8');
  }

    
  static userIdHelper(author: string, platformName: string) {
    return author + '@' + platformName;
  }
}