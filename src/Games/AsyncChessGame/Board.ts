import { CommandType, Letter, Piece } from './ChessEnums';
import { ChessPiece } from './ChessPiece';
import { Command } from './Command';

export class Board {
  static generated = false;
  static board: Map<string, ChessPiece>;
  static teams: string[];
  static sizeX = 0;
  static sizeY = 0;
  static maxX = 16;
  static maxY = 16;

  static getBoard() {
    if (!this.generated) {
      this.newBoard(8, 8, ['Black', 'White']);
      console.log(`The board isn't generated, generating a new default sized board`);
      this.generated = true;
    }
    return this.board;
  }

  static newBoard(sizeX: number, sizeY: number, teamNames: string[], kingColumn?: number, kingRow?: number) {
    this.board = new Map<string, ChessPiece>();
    if(!Number.isSafeInteger(kingColumn)) kingColumn = sizeX / 2;
    if(!Number.isSafeInteger(kingRow)) kingRow = 1;
    if(sizeX > this.maxX || sizeY > this.maxY) throw new Error(`Attempt to make chess board size ${sizeX}, ${sizeY} which is larger than max size ${this.maxX}, ${this.maxY}`);
    this.teams = teamNames;
    this.sizeX = sizeX;
    this.sizeY = sizeY;
    for (let i = 1; i <= sizeX ; i++) {
      for (let j = 1; j <= sizeY ; j++) {
        this.board.set(this.coordinateName(i, j), new ChessPiece(Piece.Empty));
      }
    }
    this.printBoard();
    this.addPiece(new ChessPiece(Piece.King, this.teams[0]), this.coordinateName(kingColumn, kingRow));
    this.addPiece(new ChessPiece(Piece.King, this.teams[1]), this.coordinateName(kingColumn, sizeY - kingRow + 1));
    this.generated = true;
  }

  static change(cmd: Command) {
    if(cmd.type === CommandType.Add) {
      this.addPiece(cmd.pieceType, cmd.to);
    } else if (cmd.type === CommandType.Move) {
      this.movePiece(cmd.from, cmd.to);
    }
  }

  static isGameWon() {
    let team0KingCount = 0;
    let team1KingCount = 0;
    for (let i = 1; i <= this.sizeX; i++) {
      for (let j = 1; j <= this.sizeY; j++) {
        const piece = Board.board.get(Board.coordinateName(i, j));
        if(piece.pieceType === Piece.King) {
          if (piece.team === Board.teams[0]) team0KingCount++;
          if (piece.team === Board.teams[1]) team1KingCount++;
        }
      }
    }
    if(team0KingCount === 0 && team1KingCount === 0) return 2;
    if(team0KingCount === 0) return 1;
    if(team1KingCount === 0) return 0;
    return -1;
  }

  static movePiece(from: string, to: string, pieceType?: Piece) {
    console.log(`Trying to move ${Board.board.get(from).pieceType} from ${from} to ${to}`);
    let finalCord = to;
    if(pieceType !== undefined && pieceType !== Piece.Knight) {
      finalCord = Board.clearLine(from, to);
    }
    Board.getBoard().set(finalCord, Board.getBoard().get(from));
    Board.getBoard().set(from, new ChessPiece(Piece.Empty));
  }

  static clearLine(from, to) {
    const [fromX, fromY] = Board.coordinateNum(from);
    const [toX, toY] = Board.coordinateNum(to);
    const distance = Math.max(Math.abs(fromX - toX), Math.abs(fromY - toY));

    const piece = Board.getBoard().get(from);
    const moves = piece.validMoves(fromX, fromY, piece.team);
    if(!moves.includes(to)) {
      for (let i = 0, x = toX, y = toY; i < distance; i++) {
        x += (fromX - toX) / distance;
        y += (fromY - toY) / distance;
        const cord = Board.coordinateName(x, y);
        if(moves.includes(cord)) return cord;
      }
    } else {
      return to;
    }
  }

  static addPiece(piece: ChessPiece, coordinate: string) {
    console.log(`Trying to add ${piece.pieceType} to ${coordinate}`);
    if(this.board.get(coordinate).pieceType === Piece.Empty) {
      this.board.set(coordinate, piece);
      return true;
    }
    return false;
  }

  static coordinateName(x: number, y: number) {
    return '' + Letter[x] + y;
  }

  static coordinateNum(cord: string) {
    return [Letter[cord[0]], cord[1]];
  }

  static at(coordinate: string) {
    return this.board.get(coordinate);
  }

  static printBoard() {
    let msg = '   '; //initial spacing

    //Board Coordinate Letters on top
    for (let i = 1; i <= this.sizeX; i++) {
      msg += ` ${Letter[i]} |`;
    }

    //Add a separating line between the coordinate letters and the board
    msg += `\n———`;
    for (let i = 1; i <= this.sizeX; i++) {
      msg += '————';
    }
    msg += `\n`;

    //Iterate through each Row
    for (let j = this.sizeY; j > 0; j--) {
      //Add Row Coordinate Numbers which can be one or two characters if over 10
      msg += `${j}`;
      if(j.toString().length == 1) msg += ' ';
      msg += '|';

      //Go through each column in the row and get the piece shorthand in that square if applicable.
      for (let i = 1; i <= this.sizeX; i++) {
        msg += ' ' + this.board.get(this.coordinateName(i, j)).pieceShorthand + ' |';
      }

      //Add a separating line between board rows
      msg += `\n———`;
      for (let i = 1; i <= this.sizeX; i++) {
        msg += '————';
      }
      msg += `\n`;

    }

    //Board Coordinate Letters on bottom
    msg += '   ';
    for (let i = 1; i <= this.sizeX; i++) {
      msg += ` ${Letter[i]} |`;
    }
    console.log(msg);
  }
}
