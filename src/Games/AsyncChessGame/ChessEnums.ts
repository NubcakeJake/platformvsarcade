
export enum CommandType{
  Move,
  Add
}

export enum Piece {
  'OutOfBounds' = '',
  'Empty' = ' ',
  'King' = 'King',
  'Queen' = 'Queen',
  'Knight' = 'Knight',
  'Bishop' = 'Bishop',
  'Rook' = 'Rook',
  'Pawn' = 'Pawn'
}

export enum Cost {
  Pawn=50,
  Knight=100,
  Bishop=150,
  Rook = 150,
  Queen=200,
  King=100
}

// noinspection JSUnusedGlobalSymbols
export enum Letter {
  A = 1,
  B,
  C,
  D,
  E,
  F,
  G,
  H,
  I,
  J,
  K,
  L,
  M,
  N,
  O,
  P
}