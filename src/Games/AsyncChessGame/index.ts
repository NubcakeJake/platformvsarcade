import { setTimeout } from 'timers/promises';
import Heap from 'heap-js';
import { IChatModule, Message } from '../../@types/IChatModule';
import { User } from '../../Users';
import { Game } from '../Game';
import { Board } from './Board';
import { Letter } from './ChessEnums';
import { Command } from './Command';

export class AsyncChessGame extends Game {
  name = 'Asynchronous Chess!';
  maxTeams = 2;
  commandHeap = new Heap<Command>(this.cmp);
  boardX = 8;
  boardY = 8;
  running;


  cmp(a, b) {
    if(a.points > b.points) {
      return -1;
    }
    if(a.points < b.points) {
      return -1;
    }
  }

  constructor(platform1: IChatModule, platform2: IChatModule) {
    super({
      [platform1.name]: platform1,
      [platform2.name]: platform2
    });

    Board.newBoard(this.boardX, this.boardY, [platform1.name, platform2.name]);
  }

  async run() {
    this.running = true;
    let tickTimer;
    while(this.running) {
      console.log('Round Begin');
      let roundTicking = true;
      setTimeout(30000).finally(() => {roundTicking = false;});
      while (roundTicking) {
        tickTimer = setTimeout(10000);
        console.log('tick');
        await this.tick();
        await tickTimer;
      }
      while(this.commandHeap.size() > 0) {
        this.popCommand();
      }
      if(Board.isGameWon() >= 0) {
        console.log(`Game is won by team ${Board.isGameWon()}`);
        this.running = false;
      }
    }
  }

  async tick() {
    await this.processMessages();
    Board.printBoard();
  }

  popCommand() {
    //TODO process the highest point command in the commandMap
    const execute = this.commandHeap.pop();
    const incompatibleCommands = this.commandHeap.toArray().filter((el) => {
      return el.from === execute.from;
    });
    for (const incompatibleCommand of incompatibleCommands) {
      this.commandHeap.remove(incompatibleCommand);
    }
    Board.change(execute);
  }

  processCommand(team: string, msg: Message) {
    let commandBreakdown: string[];
    let vars: any;
    const commandingUser = this.users.get(Game.userIdHelper(msg.author, team));
    const oldPoints = commandingUser.watchTimePoints;
    let fromX: number, fromY: number, toX: number, toY: number;
    let extraPoints = 0;
    let piece;
    let legalMove = true;

    function addMove(commandHeap, fromX, fromY, toX, toY, user: User, extraPoints?) {
      const from = Board.coordinateName(fromX, fromY);
      const to = Board.coordinateName(toX, toY);
      const command = from + to;

      let spend = Board.board.get(from).moveCost(toX, toY);
      if(user.watchTimePoints > spend + extraPoints) spend += extraPoints;

      const existingCmd = commandHeap.toArray().find((c) => c.name === command);
      if(existingCmd === undefined) {
        console.log(`${command} making new command.`);
        const cmd = new Command(from, to, spend);
        commandHeap.push(cmd);
      } else {
        console.log(`${command} adding points.`);
        existingCmd.addPoints(spend);
        commandHeap.check();
      }
      console.log(`${command} processing complete.`);
      user.spendPoints(spend);
    }

    if(msg.message[0] !== '!') {
      return;
    } else {
      commandBreakdown = msg.message.split(' ', 10);
    }
    switch (commandBreakdown[0]) {
    case '!move':
      if (commandBreakdown[1].length === 2) {
        fromX = Letter[commandBreakdown[1].toUpperCase()[0]];
        fromY = Number.parseInt(commandBreakdown[1][1]);
      }
      if (commandBreakdown[2].length === 2) {
        toX = Letter[commandBreakdown[2].toUpperCase()[0]];
        toY = Number.parseInt(commandBreakdown[2][1]);
      }
      if (commandBreakdown.length > 3) {
        extraPoints += Number.parseInt(commandBreakdown[3]);
        extraPoints = Math.floor(Math.abs(extraPoints));
      }
      piece = Board.getBoard().get(Board.coordinateName(fromX, fromY));
      console.log(piece);
      console.log(commandingUser.watchTimePoints > piece.moveCost(toX, toY));
      console.log(`${commandingUser.id} has ${commandingUser.watchTimePoints} and is trying to spend ${piece.moveCost(toX, toY)}`);
      legalMove &&= piece.validMoves(fromX, fromY, team).includes(Board.coordinateName(toX, toY));
      legalMove &&= commandingUser.watchTimePoints > piece.moveCost(toX, toY);
      if (legalMove) addMove(this.commandHeap, fromX, fromY, toX, toY, commandingUser, extraPoints);
      console.log(legalMove);
      break;

    case '!wasteX':
      vars = Number.parseInt(commandBreakdown[1]);
      if (vars) {
        if (commandingUser.watchTimePoints > vars) {
          commandingUser.spendPoints(vars);
          console.log(`@${commandingUser.username} wasted ${vars} points bringing them from ${oldPoints} to ${commandingUser.watchTimePoints} points.`);
        } else {
          console.log(`@${commandingUser.username} tried to waste ${vars} points, but only has ${commandingUser.watchTimePoints} points!`);
        }
      } else {
        console.log(`@${commandingUser.username} tried to waste some points, but ${commandBreakdown[1]} is not a number!`);
        this.platforms[team].sendMessage(`@${msg.author} arg0 for command !wasteX is Not A Number`);
      }
      break;

    case '!points':
      this.platforms[team].sendMessage(`@${msg.author} You have ${commandingUser.watchTimePoints} points, and you have ${commandingUser.lifetimeWatchTimePoints} lifetime points`);
      break;
    }
  }
}
