import { Board } from './Board';
import { Cost, Piece } from './ChessEnums';

export class ChessPiece {
  pieceType: Piece;
  pieceShorthand: string;
  team: string;

  constructor(piece: Piece, team?: string) {
    this.pieceType = piece;
    this.team = team ?? 'None';
    this.pieceShorthand = this.pieceType[0];
    if(this.pieceType === Piece.Knight) this.pieceShorthand = 'N';
  }

  validMoves(x: number, y: number, team: string): string[] {
    const moves = [];
    function test(cord: string) {
      const piece = Board.at(cord);
      if (piece === undefined) return false;
      if (piece.team === team) return false;
      if (piece.pieceType === Piece.OutOfBounds) return false; // noinspection RedundantIfStatementJS
      return true;
    }

    function plusTest() {
      let cord;
      let last = true;
      for (let i = y + 1; i < Board.sizeY && last; i++) { //Vertical - Up
        cord = Board.coordinateName(x, i);
        if (test(cord)) moves.push(cord) ;
        else last = false;
      }

      last = true;
      for (let i = x - 1; i >= 0 && last; i--) { //Vertical - Down
        cord = Board.coordinateName(x, i);
        if (test(cord)) moves.push(cord) ;
        else last = false;
      }
      last = true;
      for (let i = x + 1; i < Board.sizeX && last; i++) { //Horizontal - Right
        cord = Board.coordinateName(i, y);
        if (test(cord)) moves.push(cord) ;
        else last = false;
      }

      last = true;
      for (let i = x - 1; i >= 0 && last; i--) { //Horizontal - Left
        cord = Board.coordinateName(i, y);
        if (test(cord)) moves.push(cord) ;
        else last = false;
      }
    }

    function xTest() {
      let cord;
      let last = true;
      for (let i = x + 1, j = y + 1; i < Board.sizeX && j < Board.sizeY && last; i++, j++) { //NorthEast
        cord = Board.coordinateName(i, j);
        if (test(cord)) moves.push(cord) ;
        else last = false;
      }

      last = true;
      for (let i = x - 1, j = y + 1; i >= 0 && j < Board.sizeY && last; i--, j++) { //NorthWest
        cord = Board.coordinateName(i, j);
        if (test(cord)) moves.push(cord) ;
        else last = false;
      }

      last = true;
      for (let i = x + 1, j = y - 1; i < Board.sizeX && j >= 0 && last; i++, j--) { //SouthEast
        cord = Board.coordinateName(i, j);
        if (test(cord)) moves.push(cord) ;
        else last = false;
      }

      last = true;
      for (let i = x - 1, j = y - 1; i >= 0 && j >= 0 && last; i--, j--) { //SouthWest
        cord = Board.coordinateName(i, j);
        if (test(cord)) moves.push(cord) ;
        else last = false;
      }
    }

    if(this.pieceType === Piece.OutOfBounds) return [];
    if(this.pieceType === Piece.Empty) return [];
    if(this.pieceType === Piece.King) {
      let cord;
      cord = Board.coordinateName(x - 1, y - 1);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x - 1, y);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x - 1, y + 1);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x, y - 1);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x, y + 1);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x + 1, y - 1);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x + 1, y);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x + 1, y + 1);
      if (test(cord)) moves.push(cord);
    }
    if(this.pieceType === Piece.Queen) {plusTest(); xTest();}
    if(this.pieceType === Piece.Rook) plusTest();
    if(this.pieceType === Piece.Bishop) xTest();
    if(this.pieceType === Piece.Pawn) {
      let cord;
      if (this.team === Board.teams[0]) {
        //Move
        cord = Board.coordinateName(x, y + 1);
        if (test(cord)) moves.push(cord);
        //Attack
        cord = Board.coordinateName(x - 1, y + 1);
        if (test(cord) && Board.board.get(cord).team === Board.teams[1]) moves.push(cord);
        cord = Board.coordinateName(x + 1, y + 1);
        if (test(cord) && Board.board.get(cord).team === Board.teams[1]) moves.push(cord);
      } else if (this.team === Board.teams[1]) {
        //Move
        cord = Board.coordinateName(x, y - 1);
        if (test(cord)) moves.push(cord);
        //Attack
        cord = Board.coordinateName(x - 1, y - 1);
        if (test(cord) && Board.board.get(cord).team === Board.teams[0]) moves.push(cord);
        cord = Board.coordinateName(x + 1, y - 1);
        if (test(cord) && Board.board.get(cord).team === Board.teams[0]) moves.push(cord);
      }
    }
    if(this.pieceType === Piece.Knight) {
      let cord;
      cord = Board.coordinateName(x - 2, y - 1);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x - 2, y + 1);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x - 1, y + 2);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x + 1, y + 2);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x + 2, y - 1);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x + 2, y + 1);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x - 1, y - 2);
      if (test(cord)) moves.push(cord);

      cord = Board.coordinateName(x + 1, y - 2);
      if (test(cord)) moves.push(cord);
    }
    return moves;
  }

  moveCost(toX: number, toY: number) {
    console.log(`Base cost: ${this.baseCost()}`);
    if(this.team === Board.teams[0]) {
      return this.baseCost() * toY;
    }
    if(this.team === Board.teams[1]) {
      return this.baseCost() * (Board.sizeY - toY + 1);
    }
    return this.baseCost();//FIXME this function falls through to this return every time.
  }

  buyCost(toX: number, toY: number) {
    const pieceCost = this.baseCost() * 5;
    let multiplier = 1;
    if(this.team === Board.teams[0]) {
      multiplier *= toY;
    }
    if(this.team === Board.teams[1]) {
      multiplier *= (Board.sizeY - toY + 1);
    }
    return pieceCost * multiplier;
  }

  baseCost() {
    switch (this.pieceType) {
    case Piece.Pawn:
      return Cost.Pawn;

    case Piece.Knight:
      return Cost.Knight;

    case Piece.Bishop:
      return Cost.Bishop;

    case Piece.Rook:
      return Cost.Rook;

    case Piece.Queen:
      return Cost.Queen;

    case Piece.King:
      return Cost.King;

    }
  }
}
