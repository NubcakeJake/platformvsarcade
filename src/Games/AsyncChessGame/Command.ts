import { CommandType } from './ChessEnums';
import { ChessPiece } from './ChessPiece';

export class Command {
  name: string;
  from: string;
  to: string;
  points: number;
  type: CommandType;
  pieceType: ChessPiece;

  constructor(from, to, points, pieceType?: ChessPiece) {
    this.name = from + to;
    this.from = from;
    this.to = to;
    this.points = points;
    if (pieceType === undefined) {
      this.type = CommandType.Move;
    } else {
      this.type = CommandType.Add;
      this.pieceType = pieceType;
    }

  }

  addPoints(points) {
    this.points += points;
  }
}

