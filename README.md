## What does it do?
This application is a service that runs an arcade of games which are streamed to multiple live video services (i.e. Twitch, Youtube, etc.) and interacted with through the respective live chats.  In most games users on the same platform will be on the same team playing against users on other platforms, but this doesn't have to be the case!

## Why do it?
This is my first TypeScript project so I wanted to use this as an opportunity to explore the language and the tools it brings.  As far as the specific functionality of the app it is an interesting idea that I've been toying around with for a while.  I think the social aspect of interacting in a live chat will bring interesting challenges and strategies to the gameplay.  While my original vision for this was to have a singular long-form game that is played out over the course of a day or longer, I would like to start by making smaller games on the platform as proof of concept items before tackling the game design aspects associated with a much longer running game.

## What are my key educational goals with this project?
Ultimately I would like to use the best tool for the job in the final state of the project, but in the interim I am trying to utilize and integrate TypeScript, Node.js, some currently undecided NoSQL database, some cloud host such as AWS or Azure.

## Future-proofing?
This app is not all architechted up front, but while making it I am making an effort to make it extensible so that new platforms can be added as teams/players and that new games can be added later.  Surely requirements and specifications will change over time, but some attention is being put on how to keep future options open by standardizing interfaces between the logic running the arcade, the games, and the platform service handlers.

## Contributions?
I am working on this in large part to learn about certain technologies I have and have not worked with before, but if you spot a bug, want to make a contribution, want to write a game on this platform, or have any other questions feel free to contact me!
